
FROM tomcat:latest

# Pasta de trabalho dentro da imagem
WORKDIR /usr/local/tomcat/webapps

# Copiar o war file gerado durante a stage de build
COPY ./JSPSample-0.0.2.war .


EXPOSE 8080
